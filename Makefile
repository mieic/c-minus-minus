## File generated by the BNF Converter (bnfc 2.9.4).

# Makefile for building the parser and test program.

AGDA       = agda
GHC        = ghc
GHC_OPTS   = -dynamic
HAPPY      = happy
HAPPY_OPTS = --array --info --ghc --coerce
ALEX       = alex
ALEX_OPTS  = --ghc

# List of goals not corresponding to file names.

.PHONY : all clean distclean

# Default goal.

all : CC/Test Main

# Rules for building the parser.

CC/Abs.hs CC/Lex.x CC/Par.y CC/Print.hs CC/Test.hs CC/AST.agda CC/Parser.agda CC/IOLib.agda CC/Main.agda : CC.cf
	bnfc --haskell -d --agda CC.cf

%.hs : %.y
	${HAPPY} ${HAPPY_OPTS} $<

%.hs : %.x
	${ALEX} ${ALEX_OPTS} $<

CC/Test : CC/Abs.hs CC/Lex.hs CC/Par.hs CC/Print.hs CC/Test.hs
	${GHC} ${GHC_OPTS} $@

Main : CC/Main.agda CC/AST.agda CC/Parser.agda CC/IOLib.agda CC/ErrM.hs CC/Lex.hs CC/Par.hs CC/Print.hs
	${AGDA} --no-libraries --ghc --ghc-flag=-Wwarn --ghc-flag=-dynamic $<

# Rules for cleaning generated files.

clean :
	-rm -f CC/*.hi CC/*.o CC/*.log CC/*.aux CC/*.dvi CC/*.agdai
	-rm -rf MAlonzo

distclean : clean
	-rm -f CC/Abs.hs CC/Abs.hs.bak CC/ComposOp.hs CC/ComposOp.hs.bak CC/Doc.txt CC/Doc.txt.bak CC/ErrM.hs CC/ErrM.hs.bak CC/Layout.hs CC/Layout.hs.bak CC/Lex.x CC/Lex.x.bak CC/Par.y CC/Par.y.bak CC/Print.hs CC/Print.hs.bak CC/Skel.hs CC/Skel.hs.bak CC/Test.hs CC/Test.hs.bak CC/XML.hs CC/XML.hs.bak CC/AST.agda CC/AST.agda.bak CC/Parser.agda CC/Parser.agda.bak CC/IOLib.agda CC/IOLib.agda.bak CC/Main.agda CC/Main.agda.bak CC/CC.dtd CC/CC.dtd.bak CC/Test CC/Lex.hs CC/Par.hs CC/Par.info CC/ParData.hs Main
	-rmdir -p CC/

# EOF
